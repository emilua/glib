#include <emilua_glib/plugin.hpp>

namespace emilua_glib {

void plugin::init_appctx(
    const std::unique_lock<std::shared_mutex>&, emilua::app_context& appctx
) noexcept {
    appctx.init_log_domain<log_domain>();
}

} // namespace emilua_glib

emilua_glib::plugin EMILUA_PLUGIN_SYMBOL;
