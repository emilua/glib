/* Copyright (c) 2021, 2023 Vinícius dos Santos Oliveira

   Distributed under the Boost Software License, Version 1.0. (See accompanying
   file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt) */

#include <emilua_glib/service.hpp>
#include <emilua_glib/plugin.hpp>

#include <boost/asio/posix/descriptor.hpp>
#include <boost/asio/bind_executor.hpp>
#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <system_error>

#include <syslog.h>

// GMainLoop iterations are based on poll() APIs. That means a stateless model
// at each iteration step. That means GMainLoop will close fds on the interest
// set behind our back. Therefore we could access dangling fds if we try to be
// too smart by reusing the interest set from the previous iteration step. Don't
// try to be smart.
//
// The only robust solution is to clear the interest set before we dispatch the
// ready handlers. That's rather costful but you're not worried about efficiency
// if you're using GLib.

// A fallback timeout value to be used during high load scenarios where fd watch
// requests cannot be fulfilled.
#define SERVICE_FALLBACK_TIMEOUT_SECS 30

#define LOG(...) appctx.log<log_domain>(__VA_ARGS__)

int emilua::log_domain<emilua_glib::log_domain>::log_level = LOG_WARNING;

namespace emilua_glib {

namespace asio = boost::asio;

#ifndef NDEBUG
static gint poll_func(GPollFD* fds, guint nfds, gint timeout);
#endif // !defined(NDEBUG)

service::service(emilua::vm_context& vm_ctx)
    : emilua::pending_operation(/*shared_ownership=*/false)
    , appctx(vm_ctx.appctx)
    , strand(vm_ctx.strand())
    , main_context(
        g_main_context_new_with_flags(G_MAIN_CONTEXT_FLAGS_OWNERLESS_POLLING),
        g_main_context_unref)
    , epoll_watcher(strand.context())
{
    LOG(LOG_INFO, "service constructed {}", static_cast<void*>(this));
#ifndef NDEBUG
    g_main_context_set_poll_func(main_context.get(), poll_func);
#endif // !defined(NDEBUG)

    int epfd = epoll_create1(/*flags=*/0);
    if (epfd == -1)
        throw std::system_error(errno, std::system_category());

    boost::system::error_code ec;
    epoll_watcher.assign(epfd, ec);
    if (ec) {
        close(epfd);
        throw std::system_error(static_cast<std::error_code>(ec));
    }

    timerfd = timerfd_create(CLOCK_MONOTONIC, 0);
    if (timerfd == -1)
        throw std::system_error(errno, std::system_category());

    struct epoll_event ev;
    ev.events = EPOLLIN;
    if (epoll_ctl(epfd, EPOLL_CTL_ADD, timerfd, &ev) == -1)
        throw std::system_error(errno, std::system_category());

    fds.resize(1);
}

service::service(emilua::vm_context& vm_ctx, GMainContext* main_context)
    : service{vm_ctx}
{
    using std::swap;
    std::unique_ptr<GMainContext, void(*)(GMainContext*)> new_main_context{
        main_context, [](GMainContext*) { /* no-op */ }};
    swap(this->main_context, new_main_context);
}

service::~service()
{
    LOG(LOG_INFO, "service destructed {}", static_cast<void*>(this));
    if (timerfd != -1)
        close(timerfd);
}

void service::start()
{
    if (main_context.get_deleter() != g_main_context_unref) {
        // we don't manage GMainContext
        return;
    }

    assert(strand.running_in_this_thread());
    if (running) {
        // There still is a g_main_context_wakeup() scheduled wakeup. Nothing to
        // do as that wakeup will see a work_count bigger than 0 and call
        // prepare_iteration() already.
        LOG(LOG_DEBUG, "{} start(): previous work_count=0 running=true",
            static_cast<void*>(this));
        return;
    };

    LOG(LOG_DEBUG, "{} start(): posting prepare_iteration()",
        static_cast<void*>(this));
    running = true;
    prepare_iteration();
}

void service::stop()
{
    if (main_context.get_deleter() != g_main_context_unref) {
        // we don't manage GMainContext
        return;
    }

    assert(strand.running_in_this_thread());
    LOG(LOG_DEBUG, "{} stop(): new work_count=0 running={}."
        " Calling g_main_context_wakeup()",
        static_cast<void*>(this), running);
    g_main_context_wakeup(main_context.get());
}

void service::prepare_iteration()
{
    LOG(LOG_DEBUG, "{} prepare_iteration() called", static_cast<void*>(this));
    assert(strand.running_in_this_thread());
    gboolean context_acquired = g_main_context_acquire(main_context.get());
    assert(context_acquired == TRUE); (void)context_acquired;
    BOOST_SCOPE_EXIT_ALL(this) {
        g_main_context_release(main_context.get());
    };

    gboolean ready_to_dispatch = g_main_context_prepare(
        main_context.get(), &max_priority);
    LOG(LOG_DEBUG, "{} prepare_iteration(): g_main_context_prepare():"
        " ready_to_dispatch={} max_priority={}",
        static_cast<void*>(this), ready_to_dispatch, max_priority);
    gint timeout;
    for (;;) {
        fds_used_size = g_main_context_query(
            main_context.get(), max_priority, &timeout,
            fds.data(), static_cast<gint>(fds.size()));
        LOG(LOG_DEBUG, "{} prepare_iteration(): g_main_context_query():"
            " fds_used_size={} timeout={}",
            static_cast<void*>(this), fds_used_size, timeout);
        if (fds_used_size > static_cast<gint>(fds.size())) {
            LOG(LOG_DEBUG, "{} prepare_iteration():"
                " fds_used_size={} > fds.size()={}. Growing fds.",
                static_cast<void*>(this), fds_used_size, fds.size());
            fds.resize(fds_used_size);
            continue;
        }
        break;
    }
    gint nready = 0;
    if (fds_used_size != 0) {
        nready = g_poll(fds.data(), fds_used_size, /*timeout=*/0);
        LOG(LOG_DEBUG, "{} prepare_iteration(): g_poll(): nready={}",
            static_cast<void*>(this), nready);
    }

    if (ready_to_dispatch == TRUE || timeout == 0 || nready != 0) {
        LOG(LOG_DEBUG, "{} prepare_iteration(): posting check_iteration()",
            static_cast<void*>(this));
        strand.defer([this]() {
            check_iteration();
        }, std::allocator<void>{});
        return;
    }
    int epfd = epoll_watcher.native_handle();
    for (gint i = 0 ; i != fds_used_size ; ++i) {
        auto& fd = fds[i];
        struct epoll_event ev;
        ev.events = 0;
        if (fd.events & G_IO_IN) ev.events |= EPOLLIN;
        if (fd.events & G_IO_OUT) ev.events |= EPOLLOUT;
        if (fd.events & G_IO_PRI) ev.events |= EPOLLPRI;
        if (fd.events & G_IO_ERR) ev.events |= EPOLLERR;
        if (fd.events & G_IO_HUP) ev.events |= EPOLLHUP;
        if (epoll_ctl(epfd, EPOLL_CTL_ADD, fd.fd, &ev) == -1) {
            switch (errno) {
            case ENOMEM:
            case ENOSPC:
                // timer fallback for the remaining fds
                LOG(LOG_NOTICE,
                    "EPOLL_CTL_ADD operation failed ERRNO=ENOMEM|ENOSPC");
                if (timeout == -1 ||
                    SERVICE_FALLBACK_TIMEOUT_SECS * 1000 < timeout) {
                    timeout = SERVICE_FALLBACK_TIMEOUT_SECS * 1000;
                }
                i = fds_used_size - 1;
                continue;
            case ELOOP: {
                LOG(LOG_CRIT,
                    "EPOLL_CTL_ADD operation failed FD={} ERRNO=ELOOP",
                    fd.fd);
                std::abort();
                break;
            }
            default: {
                LOG(LOG_ERR,
                    "EPOLL_CTL_ADD operation failed FD={} ERRNO={}:{}",
                    fd.fd, errno, std::system_category().message(errno));
                break;
            }
            }
        }
    }
    if (timeout != -1) {
        LOG(LOG_DEBUG, "{} prepare_iteration(): arming timeout={}",
            static_cast<void*>(this), timeout);
        struct itimerspec new_value;
        new_value.it_interval.tv_sec = 0;
        new_value.it_interval.tv_nsec = 0;
        new_value.it_value.tv_sec = timeout / 1000;
        new_value.it_value.tv_nsec = (timeout % 1000) * 1'000'000;
        timerfd_settime(timerfd, /*flags=*/0, &new_value, NULL);
    }
    LOG(LOG_DEBUG, "{} prepare_iteration(): waiting on epoll fd",
        static_cast<void*>(this));
    epoll_watcher.async_wait(
        asio::posix::descriptor::wait_read,
        asio::bind_executor(
            emilua::remap_post_to_defer<emilua::strand_type>{strand},
            [this](const boost::system::error_code& ec) {
                if (ec == asio::error::operation_aborted)
                    return;

                check_iteration();
            }
        ));
}

void service::check_iteration()
{
    LOG(LOG_DEBUG, "{} check_iteration() called", static_cast<void*>(this));
    assert(strand.running_in_this_thread());
    gboolean context_acquired = g_main_context_acquire(main_context.get());
    assert(context_acquired == TRUE); (void)context_acquired;
    BOOST_SCOPE_EXIT_ALL(this) {
        g_main_context_release(main_context.get());
    };

    {
        struct itimerspec disarm_value;
        disarm_value.it_interval.tv_sec = 0;
        disarm_value.it_interval.tv_nsec = 0;
        disarm_value.it_value.tv_sec = 0;
        disarm_value.it_value.tv_nsec = 0;
        timerfd_settime(timerfd, /*flags=*/0, &disarm_value, NULL);
    }

    for (gint i = 0 ; i != fds_used_size ; ++i) {
        epoll_ctl(epoll_watcher.native_handle(), EPOLL_CTL_DEL, fds[i].fd,
                  NULL);
    }

    gint nready = g_poll(fds.data(), fds_used_size, /*timeout=*/0);
    LOG(LOG_DEBUG, "{} check_iteration(): g_poll(): nready={}",
        static_cast<void*>(this), nready);
    (void)nready;
    bool ready_to_dispatch = g_main_context_check(
        main_context.get(), max_priority, fds.data(), fds_used_size);
    LOG(LOG_DEBUG, "{} check_iteration(): g_main_context_check():"
        " ready_to_dispatch={}",
        static_cast<void*>(this), ready_to_dispatch);
    if (ready_to_dispatch) {
        g_main_context_push_thread_default(main_context.get());
        BOOST_SCOPE_EXIT_ALL(this) {
            g_main_context_pop_thread_default(main_context.get());
        };
        g_main_context_dispatch(main_context.get());
    }

    if (work_count == 0) {
        LOG(LOG_DEBUG, "{} check_iteration(): work_count=0. Leaving.",
            static_cast<void*>(this));
        running = false;
        return;
    }

    LOG(LOG_DEBUG, "{} check_iteration(): posting prepare_iteration()",
        static_cast<void*>(this));
    prepare_iteration();
}

void service::cancel() noexcept
{
    LOG(LOG_DEBUG, "{} shutdown() called", static_cast<void*>(this));
    boost::system::error_code ignored_ec;
    epoll_watcher.cancel(ignored_ec);
    main_context.reset();
}

#ifndef NDEBUG
gint poll_func(GPollFD* fds, guint nfds, gint timeout)
{
    assert(timeout == 0);
    return g_poll(fds, nfds, timeout);
}
#endif // !defined(NDEBUG)

} // namespace emilua_glib
