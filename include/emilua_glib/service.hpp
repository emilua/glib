/* Copyright (c) 2021, 2023 Vinícius dos Santos Oliveira

   Distributed under the Boost Software License, Version 1.0. (See accompanying
   file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt) */

#pragma once

#include <glib.h>
#include <boost/asio/posix/stream_descriptor.hpp>
#include <boost/asio/io_context_strand.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/scope_exit.hpp>
#include <boost/config.hpp>
#include <emilua/core.hpp>
#include <memory>

// GMainLoop has no real concept of pending work. No serious thought was given
// to lifetime management of work units and it's too late to change the
// semantics now. GMainLoop was designed by assuming that hidden threads exist
// and will post work units without ever notifying us whether further work is
// expected. This constraint translates to an event loop that never finishes
// (i.e. kinda deadlock by design).
//
// Our service ignores this bullshit and forces every plugin that may have
// pending work to hold a work guard object or else we won't schedule new
// GMainLoop iterations.

namespace emilua_glib {

struct log_domain;

class BOOST_SYMBOL_VISIBLE service : public emilua::pending_operation
{
public:
    class work_guard;

    explicit service(emilua::vm_context& vm_ctx);
    explicit service(emilua::vm_context& vm_ctx, GMainContext* main_context);
    ~service();

    template<class F>
    void run_with_thread_default(F&& f)
    {
        assert(strand.running_in_this_thread());
        auto main_context = this->main_context.get();
        g_main_context_push_thread_default(main_context);
        BOOST_SCOPE_EXIT_ALL(main_context) {
            g_main_context_pop_thread_default(main_context);
        };
        f();
    }

    emilua::app_context& appctx;

    emilua::strand_type strand;
    std::unique_ptr<GMainContext, void(*)(GMainContext*)> main_context;

private:
    void prepare_iteration();
    void check_iteration();

    void start();
    void stop();

    void cancel() noexcept override;

    // `running` is necessary to avoid a race that would break the prepare()
    // query() check() dispatch() order.
    std::size_t work_count = 0;
    bool running = false;

    gint max_priority;

    std::vector<GPollFD> fds;
    gint fds_used_size = 0;

    // posix::descriptor actually matches our semantics better, but it has a
    // protected destructor and we'd have to create a new class just to use it.
    boost::asio::posix::stream_descriptor epoll_watcher;
    int timerfd = -1;
};

// The work guard must not outlive the service
class service::work_guard
{
public:
    work_guard(service& service)
        : service_(reinterpret_cast<std::uintptr_t>(&service) | 1)
    {
        if (service.work_count++ == 0)
            service.start();
    }

    work_guard(const work_guard& o)
        : service_(o.service_)
    {
        if (!owns_work())
            return;

        ++get_service().work_count;
    }

    work_guard(work_guard&& o)
        : service_(o.service_)
    {
        o.service_ &= service_mask;
    }

    ~work_guard()
    {
        if (!owns_work())
            return;

        if (--get_service().work_count == 0)
            get_service().stop();
    }

    service& get_service()
    {
        return *reinterpret_cast<service*>(service_ & service_mask);
    }

    bool owns_work() const
    {
        return service_ & 1;
    }

    void reset()
    {
        if (!owns_work())
            return;

        BOOST_SCOPE_EXIT_ALL(this) {
            service_ &= service_mask;
        };

        if (--get_service().work_count == 0)
            get_service().stop();
    }

private:
    std::uintptr_t service_;

    static_assert(alignof(service) > 1);
    static constexpr std::uintptr_t service_mask = UINTPTR_MAX ^ 1;
};

inline service& get_service(emilua::vm_context& vm_ctx)
{
    service* svc = nullptr;

    for (auto& op: vm_ctx.pending_operations) {
        svc = dynamic_cast<service*>(&op);
        if (svc)
            break;
    }

    if (!svc) {
        svc = new service{vm_ctx};
        vm_ctx.pending_operations.push_back(*svc);
    }

    return *svc;
}

} // namespace emilua_glib

template<>
struct emilua::log_domain<emilua_glib::log_domain>
{
    static constexpr std::string_view name = "glib_service";
    static int log_level;
};
