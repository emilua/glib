#include <emilua_glib/service.hpp>
#include <emilua/plugin.hpp>

namespace emilua_glib {

class plugin : emilua::plugin
{
public:
    void init_appctx(
        const std::unique_lock<std::shared_mutex>&, emilua::app_context& appctx
    ) noexcept override;
};

} // namespace emilua_glib

extern "C" BOOST_SYMBOL_EXPORT emilua_glib::plugin EMILUA_PLUGIN_SYMBOL;
